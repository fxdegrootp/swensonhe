<?php

namespace Swensonhe\Cross\Cron;

use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Catalog\Api\Data\ProductLinkInterfaceFactory;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\Product;

class FillRelation
{
    /** @var array */
    protected $conditions = [
        'Machine' => [
            'filters' => [
                'product_type'     => 'eq',
                'attribute_set_id' => 'neq'
            ],
            'group' => 'coffee_flavor',
            'sort'  => 'price'
        ],
        'Pod' => [
            'filters' => [
                'product_type'     => 'eq',
                'coffee_flavor'    => 'eq',
                'attribute_set_id' => 'eq'
            ]
        ]
    ];

    /** @var ProductRepository */
    protected $productRepository;

    /** @var CollectionFactory */
    protected $collectionFactory;

    /** @var AttributeSetRepositoryInterface */
    protected $attributeSetRepo;

    /** @var ProductLinkInterfaceFactory */
    protected $productLinkInterface;

    /**
     * FillRelation constructor.
     * @param ProductRepository               $productRepository
     * @param CollectionFactory               $collectionFactory
     * @param AttributeSetRepositoryInterface $attributeSetRepo
     * @param ProductLinkInterfaceFactory     $productLinkInterface
     */
    public function __construct(
        ProductRepository $productRepository,
        CollectionFactory $collectionFactory,
        AttributeSetRepositoryInterface $attributeSetRepo,
        ProductLinkInterfaceFactory $productLinkInterface
    ) {
        $this->productRepository = $productRepository;
        $this->collectionFactory = $collectionFactory;
        $this->attributeSetRepo = $attributeSetRepo;
        $this->productLinkInterface = $productLinkInterface;
    }

    public function execute()
    {
        $collection = $this->collectionFactory->create()->addAttributeToSelect('*');

        /** @var Product $product */
        foreach ($collection as $product) {
            $linkData = [];
            $candidates = $this->collectionFactory->create();

            $attributeSetName = $this->attributeSetRepo->get($product->getAttributeSetId())->getAttributeSetName();

            if (isset($this->conditions[$attributeSetName])) {
                $linkedProducts = $this->filterProducts($candidates, $product, $attributeSetName);
                $linkData = $this->getLinkData($linkedProducts, $product);
            }

            if ($linkData) {
                $product->setProductLinks($linkData);
                $this->productRepository->save($product);
            }
        }
    }

    /**
     * Gets array of link data based on collection of products and product
     *
     * @param ProductCollection $linkedProducts
     * @param Product           $product
     *
     * @return array
     */
    protected function getLinkData($linkedProducts, $product)
    {
        $linkData = [];

        foreach ($linkedProducts as $linkProduct) {
            $link = $this->productLinkInterface->create();

            $link->setLinkedProductSku($linkProduct->getSku())
                 ->setSku($product->getSku())
                 ->setLinkType('related');
            $linkData[] = $link;
        }

        return $linkData;
    }

    /**
     * Gets filtered product collection
     *
     * @param ProductCollection $candidates
     * @param Product           $product
     * @param string            $attributeSetName
     *
     * @return ProductCollection
     */
    protected function filterProducts($candidates, $product, $attributeSetName)
    {
        $conditions = $this->conditions[$attributeSetName];

        if (isset($conditions['filters'])) {
            foreach ($conditions['filters'] as $attribute => $comparison) {
                $candidates->addAttributeToFilter(
                    $attribute,
                    [$comparison => $product->getData($attribute)]
                );
            }
        }

        if (isset($conditions['group'])) {
            $candidates->groupByAttribute($conditions['group']);
        }

        if (isset($conditions['sort'])) {
            $candidates->setOrder($conditions['sort']);
        }

        return $candidates;
    }
}
